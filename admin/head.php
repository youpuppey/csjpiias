    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="../img/favicon1.png" >
    <title>CSJPIIAS E-Learning</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../fontawesome/css/font-awesome.min.css" />
    <link href="../nprogress-master/nprogress.css" rel='stylesheet' />
    <link rel="stylesheet" href="../bootstrap/css/simple-sidebar.css" />
    <link rel="stylesheet" href="../css/style.css" />