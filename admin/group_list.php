<!DOCTYPE html>

<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../css/group.css">
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card minHeight" id="middleSection">
                          <div class="card-header">
                            Group
                          </div>
                          <div class="card-block frame">
                            <ul class="nav nav-tabs nav-justified textSize" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#note" role="tab">Note</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#quiz" role="tab">Quiz</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#assignment" role="tab">Assignment</a>
                              </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content textSize" style="padding:10px;border-bottom:1px solid lightgray">
                             <!-- Note -->
                                  <div class="tab-pane active" id="note" role="tabpanel">
                                        <div class="row">
                                            <div class="col-12">
                                                <textarea name="postNote" id="" class="form-control noteTextarea"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12" style="padding:15px;">
                                                <a href="" class="btn btn-info pull-left">Upload File</a>
                                                <a href="" class="btn btn-info pull-right">Post</a>
                                            </div>
                                        </div>
                                  </div>
                              <!-- Quiz -->
                                  <div class="tab-pane" id="quiz" role="tabpanel">
                                        <div class="row">
                                            <div class="col-12">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12" style="padding:15px;">
                                                <a href="" class="btn btn-info pull-left">Upload File</a>
                                                <a href="" class="btn btn-info pull-right">Post</a>
                                            </div>
                                        </div>
                                  </div>
                              <!-- Assignment -->
                                  <div class="tab-pane" id="assignment" role="tabpanel">...</div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <script>
            $("textarea").keyup(function(){
                this.style.overflow = 'hidden';
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            });
    </script>
</body>
</html>