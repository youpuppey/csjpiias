    <?php session_start(); ?>
    <?php include ('../headerAdmin.php');?>
  <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top" style="background:#2a3f54;border-bottom:1px solid #000">
  <!--<button class="navbr-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <img src="../img/admin/admin.jpg" width="30" height="30" class="d-inline-block align-top rounded-circle" alt="Profile"> <i class="fa fa-angle-down"></i>
  </button>-->

  <button class="btn dropdown-toggle navbar-toggler navbar-toggler-right border-0" type="button" id="NavbarSetting" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <img src="<?=$_SESSION['INFO_PICTURE'];?>" style="background:#fff;border:1px solid #fff" width="30" height="30" class="d-inline-block align-top rounded-circle" alt="Profile" />
  </button>
  
  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="NavbarSetting">
    <a class="dropdown-item" href="edit_profile.php">Edit Profile</a>
    <a class="dropdown-item" href="#">Change Password</a>
    <a class="dropdown-item" href="logout.php">Log out</a>
  </div>
  
  <!-- Navbar Logo -->
  <a class="navbar-brand hidden-md-down" href="#"><img src="../img/logo.png" alt="CSJPII" height="40px" width="40px"> </a>
  
  <!--Navbar Items-->
  <ul class="navbar-nav mr-auto hidden-md-down" style="text-align:center;">
      <li class="nav-item">
         <a class="nav-link" href="index.php" style="font-size:10px;"><i class="fa fa-home fa-3x"></i><span>Home</span></a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="group_list.php" style="font-size:10px;"><i class="fa fa-group fa-3x"></i><span>Groups</span></a>
      </li>
      <li class="nav-item">
         <a class="nav-link" href="#" style="font-size:10px;"><i class="fa fa-book fa-3x"></i><span>Lesson</span></a>
      </li>
  </ul>    
  

    <!--Logo when mobile-->
  <!--<div class="hidden-lg-up" style="text-align:center;float:left;clear:both;position:absolute;margin:0 auto;width:100%;z-index:-1;" href="#"><img src="../img/logo.png" alt="CSJPII" height="60px" width="60px"></div>
-->
  <div class="hidden-lg-up" style="float:left;"><a href="#" id="menu-toggle" class="btn"  style="color:#fff;"><span class="fa fa-bars"></span></a></div>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul>

      </ul>
  </div>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link hidden-md-down" href="#">
            <img src="<?=$_SESSION['INFO_PICTURE'];?>" width="30" height="30" class="d-inline-block align-top rounded-circle" alt="Profile" style="background:#fff;border:1px solid #fff">
        </a>
      </li>
      <li class="nav-item dropdown hidden-md-down">
        <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['INFO_FNAME']." ".$_SESSION['INFO_LNAME']."(admin)"; ?> <i class="fa fa-angle-down"></i></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="edit_profile.php">Edit Profile</a>
          <a class="dropdown-item" href="#">Change Password</a>
          <a class="dropdown-item" href="logout.php">Log out</a>
        </div>
      </li>
      <!--<li class="nav-item hidden-lg-up">
        <a class="nav-link" href="#">
            <a class="nav-link" href="edit_profile.php"><i class="fa fa-user fa-lg"></i> Edit Profile</a>
        </a>
      </li>
      <li class="nav-item hidden-lg-up">
        <a class="nav-link" href="#">
            <a class="nav-link" href="#"><i class="fa fa-lock fa-lg"></i> Change Password</a>
        </a>
      </li><li class="nav-item hidden-lg-up">
        <a class="nav-link" href="#">
            <a class="nav-link" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Log out</a>
        </a>
      </li>-->
    </ul>
  </div>
</nav>