<div class="col-lg-2 hidden-md-down" >
  <!-- Card for profile -->
   <div class="card profileCard">
    <div class="profileEdit">
        <a href="edit_profile.php" class="editLink"><i class="fa fa-user"></i> Update</a>
    </div>
      <div class="card-block" style="text-align:center;">
            <img src="<?=$_SESSION['INFO_PICTURE'];?>" style="background:#fff;border:1px solid #fff" width="75" height="75" class="d-inline-block align-top rounded-circle" alt="Profile" />
            <div class="card-text">Admin</div>
            <div class="card-text" style="font-size:2vh"><?=$_SESSION['INFO_FNAME']." ".$_SESSION['INFO_LNAME'];?></div>
      </div>
    </div>
    <!-- Card for Manages -->
    <div class="card">
      <div class="manageBlock">
        <div class="card-text">
            <ul class="manageList">
                <li onclick="navigate('manage_faculty.php');">
                    <a href="javascript:void(0);" title="Manage Faculty"><i class="fa fa-user fa-2x faCustom"></i> <span style="padding-left:35px">Manage Faculty</span></a>
                </li>
                <li onclick="navigate('manage_student.php');">
                    <a href="javascript:void(0);" title="Manage Students"><i class="fa fa-user fa-2x faCustom"></i> <span style="padding-left:35px">Manage Students</span></a>
                </li>
                <li onclick="navigate('manage_user.php');">
                    <a href="javascript:void(0);" title="Manage User"><i class="fa fa-user fa-2x faCustom"></i> <span style="padding-left:35px">Manage User</span></a>
                </li>
                <li onclick="navigate('manage_course.php');">
                    <a href="javascript:void(0);" title="Manage Course"><i class="fa fa-sitemap fa-2x faCustom"></i> <span style="padding-left:35px">Manage Course</span></a>
                </li>
                <li onclick="navigate('manage_semester.php');">
                    <a href="javascript:void(0);" title="Manage Semester"><i class="fa fa-sitemap fa-2x faCustom"></i> <span style="padding-left:35px">Manage Semester</span></a>
                </li>
                <li onclick="navigate('manage_acadyear.php');">
                    <a href="javascript:void(0);" title="Manage Academic Year"><i class="fa fa-sitemap fa-2x faCustom"></i> <span style="padding-left:35px">Manage A-Y</span></a>
                </li>
                <li onclick="navigate('manage_subject.php');">
                    <a href="javascript:void(0);" title="Manage Subjects"><i class="fa fa-book fa-2x faCustom"></i> <span style="padding-left:35px">Manage Subjects</span></a>
                </li>
                <li onclick="navigate('manage_quiz.php');">
                    <a href="javascript:void(0);" title="Manage Quiz"><i class="fa fa-book fa-2x faCustom"></i> <span style="padding-left:35px">Manage Quiz</span></a>
                </li>
                <li>
                    <a href="#" title="Audit Trail"><i class="fa fa-eye fa-2x faCustom"></i> <span style="padding-left:35px">Audit Trail</span></a>
                </li>
            </ul>
        </div>
      </div>
    </div>
</div>