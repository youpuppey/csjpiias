<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title">Courses <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#courseModal"><i class="fa fa-plus"></i> Add Course</button></h4>
                          </div>
                          <div class="card-block">
                            <table id="facultyTable"  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Course Code</th>
                                        <th>Description</th>
                                        <th>Course Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!--modal-->
    <div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="courseModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="courseModalLabel">Add Course</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="coursecode">Course Code</label>
                        <input type="text" class="form-control" id="coursecode" required>
                    </div>
                    <div class="form-group">
                        <label for="coursedesc">Course Description</label>
                        <input type="text" class="form-control" id="coursedesc" required>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <?php include('datatablesScript.php');?>
    <script>
        //var editor; // use a global for the submit and return data rendering in the examples
 
        $(document).ready(function() {
            /*editor = new $.fn.dataTable.Editor( {
                ajax: "php/staff.php",
                table: "#facultyTable",
                fields: [ {

                        name: "COURSE_CODE"
                    }, {
                        
                        name: "COURSE_DESCRIPTION"
                    }
                ]
            } );*/

            // Activate an inline edit on click of a table cell
            /*$('#facultyTable').on( 'click', 'tbody td:not(:first-child)', function (e) {
                editor.inline( this );
            } );*/

            $('#facultyTable').DataTable( {
                "bLengthChange": false,
                "pageLength": 5,
                "pagingType": "full",
                responsive: true,
                ajax: "php/db_course.php",
                order: [[ 0, 'asc' ]],
                columns: [
                    { data: "COURSE_ID" },
                    { data: "COURSE_CODE" },
                    { data: "COURSE_DESCRIPTION" },
                    { data: "COURSE_STATUS" },
                    { data: "COURSE_ID","render":function ( data, type, full, meta ) {
           return '<a href="edit_course.php?id='+data+'">View</a>';}}
                ]/*,
                select: {
                    style:    'os',
                    selector: 'td:first-child'
                }*/
            } );
        } );
    </script>
</body>
</html>