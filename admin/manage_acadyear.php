<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title">Faculty</h4>
                          </div>
                          <div class="card-block">
                            <table id="facultyTable"  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <?php include('datatablesScript.php');?>
    <script>
        var editor; // use a global for the submit and return data rendering in the examples
 
        $(document).ready(function() {
            editor = new $.fn.dataTable.Editor( {
                ajax: "php/staff.php",
                table: "#facultyTable",
                fields: [ {

                        name: "ACAD_YEAR_DESC"
                    },{

                        name: "ACAD_YEAR_STATUS"
                    }
                ]
            } );

            // Activate an inline edit on click of a table cell
            $('#facultyTable').on( 'click', 'tbody td:not(:first-child)', function (e) {
                editor.inline( this );
            } );

            $('#facultyTable').DataTable( {
                "bLengthChange": false,
                "pageLength": 5,
                "pagingType": "full",
                responsive: true,
                ajax: "php/db_acadyear.php",
                order: [[ 0, 'desc' ]],
                columns: [
                    { data: "ACAD_YEAR_ID" },
                    { data: "ACAD_YEAR_DESC" },
                    { data: "ACAD_YEAR_STATUS" },
                    { data: "ACAD_ID","render":function ( data, type, full, meta ) {
           return '<a href="edit_acadyear.php?id='+data+'">View</a>';}}
                ]/*,
                select: {
                    style:    'os',
                    selector: 'td:first-child'
                }*/
            } );
        } );
    </script>
</body>
</html>