<!DOCTYPE html>
<html>
<head>
    <?php include('head.php');?>
</head>
<body>
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" id="middleSection">
                          <div class="card-header">
                            Edit Profile
                          </div>
                          <div class="card-block">
                            <h4 class="card-title"><?=$_SESSION['INFO_NUM']."<br />".$_SESSION['INFO_LNAME'].", ".$_SESSION['INFO_FNAME']." ".$_SESSION['INFO_MNAME'];?></h4>
                            <hr>
                            <div class="row">
                               <div class="col-12">
                                   <div id="accordion" role="tablist" aria-multiselectable="true">
                                      <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                          <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              Information
                                            </a>
                                          </h5>
                                        </div>

                                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="card-block">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" role="tab" id="headingTwo">
                                          <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Profile Picture
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                          <div class="card-block">
                                           <center>
                                            <fieldset class="form-group">
                                                <legend>Image Preview</legend>
                                                <img src="../img/admin/admin.jpg" width="100px" height="100px" class="rounded-circle" id="bigImg" /> 100px&nbsp;
                                                <img src="../img/admin/admin.jpg" width="40px" height="40px" class="rounded-circle" id="smallImg" /> 40px
                                            <br />
                                            <div class="form-group">
                                                <form action="upload.php" method="post" enctype="multipart/form-data" id="i_file">
                                                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted">Upload an image. Only .jpg and .png file extension are allowed</small>
                                                </form>
                                            </div>

                                                <input type="submit" class="btn btn-success" value="Save Image" />
                                            </fieldset>
                                            </center>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" role="tab" id="headingThree">
                                          <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                              Password
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                          <div class="card-block">
                                            <center>
                                                <input type="text" placeholder="Old Password" class="form-control"/><br />
                                                <input type="text" placeholder="New Password" class="form-control"/><br />
                                                <input type="text" placeholder="Re-type New Password" class="form-control"/><br />
                                            </center>
                                                <button type="button" class="btn btn-success">Save</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                               </div>
                                <!---->
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <?php include('scripts.php');?>
    <script>
        $('#i_file').change( function(event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $("#bigImg").attr("src",tmppath);
            $("#smallImg").attr("src",tmppath);
        });
    </script>
</body>
</html>