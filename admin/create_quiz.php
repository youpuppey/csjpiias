<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="../css/quiz.css" />
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" id="middleSection">
                         <form action="add_quiz.php" method="POST">
                          <div class="card-header">
                            Create Quiz
                            <button type="submit" name="submit" class="btn btn-success pull-right">Save</button>
                          </div>
                          <div class="card-block" style="min-height:500px;">
                            <div class="row">
                                <div class="col-7">
                                    <br />
                                    <label class="quizText">Title</label>
                                    <input type="text" class="form-control" placeholder="Untitled Quiz" name="qtitle" /><br />
                                </div>
                                <div class="col-5">
                                    <br />
                                    <label class="quizText">Time Limit:(Minutes)</label>
                                    <input type="number" class="form-control" min=1 name="qtime"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <br />
                                    <textarea class="form-control aboutTextarea" placeholder="Description" onkeyup="fillText(this.value);" ></textarea>
                                    <input type="hidden" name="qabout" id="qabout" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <br />
                                    <label for="">Date/Time Start</label>
                                    <input type="text" id="dstart" class="form-control" name="qstart" />
                                </div>
                                <div class="col-6">
                                    <br />
                                    <label for="">Date/Time End</label>
                                    <input type="text" id="dend" class="form-control" name="qend"/><br />
                                </div>
                            </div>
                          </div>
                          </form>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <script src="../bootstrap/moment/moment.js"></script>
    <script src="../bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script>
        $(document).ready(function () {
            $('#dstart').datetimepicker({
                minDate: moment()
            });
        $('#dend').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#dstart").on("dp.change", function (e) {
            $('#dend').data("DateTimePicker").minDate(e.date);
        });
        $("#dend").on("dp.change", function (e) {
            $('#dstart').data("DateTimePicker").maxDate(e.date);
        });
        });
            $("textarea").keyup(function(){
                this.style.overflow = 'hidden';
                this.style.height = 0;
                this.style.height = this.scrollHeight + 'px';
            });
        
        function fillText(value){
                value = value.replace(/  /g, "&nbsp;&nbsp;");
                value = value.replace(/\n/g, "<br />");
                $("#qabout").val(value);
            }
    </script>
</body>
</html>