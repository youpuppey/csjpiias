<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title">Faculty<a href="create_quiz.php" class="btn btn-success pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create Quiz</a></h4>
                            
                          </div>
                          <div class="card-block">
                            <table id="facultyTable"  class="table table-striped table-hover" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Items</th>
                                        <th>Total Points</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Time Limit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      
                                   <?php
                                        require_once('../connect.php');
                                        $query="SELECT * FROM tbl_quiz as q, tbl_info as i WHERE q.INFO_ID=i.INFO_ID ORDER BY q.QUIZ_ID DESC;";
                                        $result=$conn->query($query);
                                        while($row=$result->fetch_object()){
                                            echo '<tr onclick="alert(\'Hello World!\')">';
                                            echo '<td>'.$row->QUIZ_TITLE.'</td>';
                                            echo '<td>'.$row->INFO_LNAME.', '.$row->INFO_FNAME.'</td>';
                                            $query="SELECT * FROM tbl_items WHERE QUIZ_ID=".$row->QUIZ_ID;
                                            $result2=$conn->query($query);
                                            $count=$result2->num_rows;
                                            $tp=0;
                                            while($row2=$result2->fetch_object()){
                                                $tp+=$row2->ITEM_P;
                                            }
                                            echo '<td>'.$count.'</td>';
                                            echo '<td>'.$tp.'</td>';
                                            echo '<td>'.$row->QUIZ_START.'</td>';
                                            echo '<td>'.$row->QUIZ_END.'</td>';
                                            echo '<td>'.($row->QUIZ_TIME/60).'</td>';
                                            echo '</tr>';
                                        }
                                    ?>
                                    
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <?php include('datatablesScript.php');?>
    <script>
        var editor; // use a global for the submit and return data rendering in the examples
 
        $(document).ready(function() {

            $('#facultyTable').DataTable( {
                "bLengthChange": false,
                "pageLength": 10,
                "pagingType": "full",
                responsive: true
            } );
        } );
    </script>
</body>
</html>