<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../css/quiz.css" />
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <!-- End of Navbar -->
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!-- End of Left side -->
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                      <input type="hidden" id="uid" value=<?=$_SESSION['USER_ID'];?>>
                      <input type="hidden" id="qid" value=<?=$_GET['id'];?>>
                       <div class="card" id="middleSection">
                          <div class="card-header">
                            Quiz Form | Total Points : <span id="tpoints">0</span>
                            <button type="button" class="btn btn-success pull-right" id="saveQuiz">Finish</button>
                          </div>
                          <div class="card-block formBody" style="overflow-x:auto;">
                           <div class="card-text" style="text-align:center" id="controlButtons"><button class="btn btn-success" id="addFirstBtn"><i class="fa fa-plus"></i> First Question</button> or <button class="btn btn-info">Upload Question</button><a class="helpBtn"><i class="fa fa-question"></i></a></div>
                            <div class="row">
                              <div class="col-2" id="controlList">
                              <span class="quizText">Items</span>(<span id="titems">0</span>)
                               <!--vertical tab-->
                                <ul class="nav flex-column" role="tablist" id="listItems">
                                </ul>
                                <!-- End of vertical tab -->
                                <button class="btn btn-success" id="addItem"><i class="fa fa-plus"></i></button>
                              </div>
                              
                              <div class="col-10" id="controlForm">
                                  <div class="tab-content" id="formPanel">
                                     <!-- Panel Form -->
                                      
                                      <!-- end of panel form -->
                                  </div>
                              </div>
                              
                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- End of Middle -->
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                    <!-- End of Right side -->
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <script src="../js/quiz.js"></script>
    <script>
        
        closeItem();
    
        
    </script>

</body>
</html>