<!DOCTYPE html>
<html>
<head>
    <?php include('head.php');?>
</head>
<body>
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <header>
                            <div class="d-inline-flex p-1"><h5>Dashboard</h5></div>
                            <!--<div class="d-inline-flex p-1"><h6>> Dashboard</h6></div>-->
                        </header>
                        <section>
                            <form action="upload.php" method="post" enctype="multipart/form-data" id="i_file">
                                Select image to upload:
                                <input type="file" name="fileToUpload" id="fileToUpload">
                                <input type="submit" value="Upload Image" name="submit">
                            </form>
                        </section>
                        <img id="changeMe" src="">
                        <div id="disp_tmp_path"></div>
                        <section>
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    1
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    1
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    1
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    1
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('scripts.php');?>
    <script></script>
</body>
</html>