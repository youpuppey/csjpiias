<?php
$select=$_GET['select'];
$id=$_GET['id'];
(isset($_GET['question'])?$q=$_GET['question']:$q='');
(isset($_GET['option1'])?$a=$_GET['option1']:$a='');
(isset($_GET['option2'])?$b=$_GET['option2']:$b='');
(isset($_GET['option3'])?$c=$_GET['option3']:$c='');
(isset($_GET['option4'])?$d=$_GET['option4']:$d='');
if($select==1){
?>
   
<!-- Multiple Choice -->
  <div class="form-group">
    <label>Question:</label>
      <textarea class="form-control quizTextarea" onblur="checkEmpty(this,'quizTextarea')" name="question"><?=$q;?></textarea>
  </div>
  <label>Option 1 (Correct Answer):</label>
  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
     <div class="input-group-addon">1</div>
      <input type="text" class="form-control quizChoices" onblur="checkEmpty(this,'quizChoices')" value="<?=$a;?>"name="option1" />
  </div>
  <label>Option 2:</label>
  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
     <div class="input-group-addon">2</div>
      <input type="text" class="form-control" onblur="checkEmpty(this)" value="<?=$b;?>"name="option2" />
  </div>
  <label>Option 3:</label>
  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
     <div class="input-group-addon">3</div>
      <input type="text" class="form-control" onblur="checkEmpty(this)" value="<?=$c;?>"name="option3" />
  </div>
  <label>Option 4:</label>
  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
     <div class="input-group-addon">4</div>
      <input type="text" class="form-control" onblur="checkEmpty(this)" value="<?=$d;?>"name="option4" />
  </div>
<!-- End of Multiple Choice -->
   
   
<?php
              }
if($select==2){
?>

<!-- True or False -->
  <div class="form-group">
    <label>Question:</label>
      <textarea class="form-control quizTextarea" name="question"><?=$q;?></textarea>
  </div>
  <div class="form-group">
     <label>(Correct Answer):</label>
      <select class="form-control quizChoices" name="option1">
          <option value="0" <?php if($a==0){echo "selected";} ?> >False</option>
          <option value="1" <?php if($a==1){echo "selected";} ?> >True</option>
      </select>
  </div>
<!-- End of True or False -->

<?php    
              }
if($select==3){
?>
<!-- FIB -->
 <span class="quizText">Note: Use ' _ ' to specify where you would like a blank to appear in the text below. Then enter the correct answer in the textbox.</span>
  <div class="form-group">
    <label>Question:</label>
      <textarea class="form-control quizTextarea" onkeyup="fillText('<?=$id;?>',this.value)"  name="question"><?=$q;?></textarea>
  </div>
  <p id="fill<?=$id;?>"></p>

<!-- End of FIB -->
<?php    
              }
?>