<!DOCTYPE html>

<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" id="middleSection">
                          <div class="card-header">
                            Dashboard1
                          </div>
                          <div class="card-block">
                            <h4 class="card-title">Dashboard</h4>
                            <div class="card-text">With supporting text below as a natural lead-in to additional content.</div>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
</body>
</html>