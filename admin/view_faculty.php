<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../css/admin.css">
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                      <?php
                        require_once('../connect.php');
                        $fname=$lname=$mname=$address=$contact=$gender=$email=$bday=$gname=$gcontact=$empid=$type="N/A";
                        if(isset($_GET['id'])){
                            $id=$_GET['id'];
                            $sql="SELECT * FROM tbl_info WHERE INFO_ID='$id'";
                            $result = $conn->query($sql);
                            $row=$result->fetch_object();
                            $fname=$row->INFO_FNAME;
                            $mname=$row->INFO_MNAME;
                            $lname=$row->INFO_LNAME;
                            $address=$row->INFO_ADDRESS;
                            $gender=$row->INFO_GENDER;
                            $bday=$row->INFO_BDAY;
                            $contact=$row->INFO_CONTACT_NUM;
                            $gname=$row->INFO_GUARDIAN_NAME;
                            $gcontact=$row->INFO_GUARDIAN_NUMBER;
                            $empid=$row->INFO_NUM;
                            $type=$row->ROLE_ID;
                            $email=$row->INFO_EMAIL;
                            if($type==1)
                                $type="STUDENT";
                            if($type==2)
                                $type="TEACHER";
                            if($type==3)
                                $type="DEAN";
                            if($type==4)
                                $type="ADMIN";
                        }
                        ?>
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title"><?=$fname." ".substr($mname,0,1).". ".$lname;?> <a href="edit_faculty.php?id=<?=$_GET['id'];?>" class="btn btn-warning pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> EDIT</a></h4>
                          </div>
                          <div class="card-block">
                            <div class="facultyInfo">
                                <div class="row">
                                   <div class="col-12">
                                        <span style="font-weight:800;">Employment Information</span>
                                        <hr>
                                    </div>
                                    
                                    <div class="col-md-4">ID: <i class="facultyData"><?=$empid;?></i></div>
                                    
                                    <div class="col-md-8">EMP TYPE: <i class="facultyData"><?=$type;?></i></div>
                                    <div class="col-12">
                                        <span style="font-weight:800;">Personal Information</span>
                                        <hr>
                                    </div>
                                    <div class="col-md-4">FIRST NAME: <i class="facultyData"><?=$fname;?></i></div>
                                    <div class="col-md-4">MIDDLE NAME: <i class="facultyData"><?=$mname;?></i></div>
                                    <div class="col-md-4">LAST NAME: <i class="facultyData"><?=$lname;?></i></div>
                                    <div class="col-md-4">BIRTH DATE: <i class="facultyData"><?=$bday;?></i></div>
                                    <div class="col-md-4">GENDER: <i class="facultyData"><?=$gender;?></i></div>
                                    <div class="col-12">
                                        <span style="font-weight:800;">Contact Information</span>
                                        <hr>
                                    </div>
                                    <div class="col-12">ADDRESS: <i class="facultyData"><?=$address;?></i></div>
                                    <div class="col-md-6">CONTACT #: <i class="facultyData"><?=$contact;?></i></div>
                                    <div class="col-md-6">EMAIL: <i class="facultyData"><?=$email;?></i></div>
                                </div>    
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
        
</body>
</html>