<?php
$ajax = ($_SERVER[ 'HTTP_X_REQUESTED_WITH' ] === 'XMLHttpRequest');
session_start();

$added = secureFaculty($_POST);

function secureFaculty($vars) {
    
    require_once('../connect.php');
    
    $added = false;
    
    //todo regex/preg_match
    
    $input = array(
     'uid' => $conn->real_escape_string($vars['uid']),
     'fname' => $conn->real_escape_string($vars['fname']),
     'mname' => $conn->real_escape_string($vars['mname']),
     'lname' => $conn->real_escape_string($vars['lname']),
     'bday' => $conn->real_escape_string($vars['bday']),
     'gender' => $conn->real_escape_string($vars['gender']),
     'infonum' => $conn->real_escape_string($vars['infonum']),
     'type' => $conn->real_escape_string($vars['type']),
     'address' => $conn->real_escape_string($vars['address']),
     'course' => $conn->real_escape_string($vars['course']),
     'email' => $conn->real_escape_string($vars['email']),
     'contact' => $conn->real_escape_string($vars['contact']),
     'date' => date('y/m/d H:i:s'));
    
    $added = updateFaculty($input);
    //$this->post_to_db($added);
      
    
    return $added;
  }

function updateFaculty($input){
    
    require('../connect.php');
    
    $added=array();
    $uid=$input['uid'];
    $fname=$input['fname'];
    $mname=$input['mname'];
    $lname=$input['lname'];
    $gender=$input['gender'];
    $bday=$input['bday'];
    $infonum=$input['infonum'];
    $type=$input['type'];
    $course=$input['course'];
    $address=$input['address'];
    $contact=$input['contact'];
    $email=$input['email'];
    $date=$input['date'];

    $query="UPDATE tbl_info SET INFO_FNAME='$fname',INFO_MNAME='$mname',INFO_LNAME='$lname',INFO_GENDER='$gender',INFO_BDAY='$bday',INFO_NUM='$infonum',ROLE_ID='$type',INFO_ADDRESS='$address',INFO_CONTACT_NUM='$contact',INFO_EMAIL='$email',COURSE_ID='$course' WHERE INFO_ID='$uid'";
    $result=$conn->query($query);
 
    
    
    
    if($result)
        $added['response']="success";
    else
        $added['response']="fail";
    
    
    return $added;
}







if($ajax) {
  sendAjaxResponse($added);
}
else {
  sendStandardResponse($added); 
}

function sendAjaxResponse($added) {
  header("Content-Type: application/json");
  if($added) {
    header( 'Status: 201' );
    echo( json_encode($added) );
  }
  else {
    header( 'Status: 400' );
  }
}

function sendStandardResponse($added) {
  if($added) {
      header( 'Location: index.php' );
  }
  else {
    header( 'Location: index.php?error=No response from the server' );
  }
}
    
?>