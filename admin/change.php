<?php
require_once('../connect.php');
$alert=$err_fname=$err_lname=$err_contact=$err_bday=$err_gender=$err_address=$err_nguardian=$err_cguardian="";
if(isset($_POST['editInfo'])){
    $fname=$conn->real_escape_string(strtoupper($_POST['fname']));
    $mname=$conn->real_escape_string(strtoupper($_POST['mname']));
    $lname=$conn->real_escape_string(strtoupper($_POST['lname']));
    $contact=$conn->real_escape_string(strtoupper($_POST['contact']));
    $bday=$conn->real_escape_string(strtoupper($_POST['bday']));
    $gender=$conn->real_escape_string(strtoupper($_POST['gender']));
    $address=$conn->real_escape_string(strtoupper($_POST['address']));
    $nguardian=$conn->real_escape_string(strtoupper($_POST['nguardian']));
    $cguardian=$conn->real_escape_string(strtoupper($_POST['cguardian']));
        
    $update="UPDATE tbl_info SET INFO_FNAME='$fname',INFO_MNAME='$mname',INFO_LNAME='$lname',INFO_CONTACT_NUM='$contact',INFO_BDAY='$bday',INFO_GUARDIAN_NAME='$nguardian',INFO_GUARDIAN_NUMBER='$nguardian',INFO_GENDER='$gender',INFO_ADDRESS='$address' WHERE INFO_ID=".$_SESSION['INFO_ID'];
    
    $emptyFields="";
    $notEmpty=true;
    
    if(empty($fname)){
        $notEmpty=false;
        $emptyFields.="<li>First Name</li>";
        $err_fname="errorField";
    }
    if(empty($lname)){
        $notEmpty=false;
        $emptyFields.="<li>Last Name</li>";
        $err_lname="errorField";
    }
    if(empty($contact)){
        $notEmpty=false;
        $emptyFields.="<li>Contact Number</li>";
        $err_contact="errorField";
    }
    if(empty($bday)){
        $notEmpty=false;
        $emptyFields.="<li>Birthdate</li>";
        $err_bday="errorField";
    }
    if(empty($gender)){
        $notEmpty=false;
        $emptyFields.="<li>Gender</li>";
        $err_gender="errorField";
    }
    if(empty($address)){
        $notEmpty=false;
        $emptyFields.="<li>Address</li>";
        $err_address="errorField";
    }
    if(empty($nguardian)){
        $notEmpty=false;
        $emptyFields.="<li>Name of Guardian</li>";
        $err_nguardian="errorField";
    }
    if(empty($cguardian)){
        $notEmpty=false;
        $emptyFields.="<li>Guardian Contact #</li>";
        $err_cguardian="errorField";
    }
    
    if($notEmpty==true){
        $resultUpdate=$conn->query($update);
    }else{
        $alert="<div class='alert alert-danger' role='alert'>
  				<a href='#' class='alert-link'>Please fill up the following fields:</a><br /><br />".$emptyFields."
				</div>";
    }
    

?>