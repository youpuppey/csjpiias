<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'tbl_info','INFO_ID' )
	->fields(
		Field::inst( 'tbl_info.INFO_ID' ),
        Field::inst( 'tbl_info.INFO_NUM' ),
        Field::inst( 'tbl_info.INFO_LNAME' ),
        Field::inst( 'tbl_info.INFO_FNAME' ),
        Field::inst( 'tbl_info.INFO_MNAME' ),
        Field::inst( 'tbl_info.INFO_STATUS' ),
        Field::inst( 'tbl_course.COURSE_CODE' )
        
	)
    ->leftJoin( 'tbl_user', 'tbl_user.INFO_ID', '=', 'tbl_info.INFO_ID' )
    ->leftJoin( 'tbl_course', 'tbl_course.COURSE_ID', '=', 'tbl_info.COURSE_ID' )
    ->where('ROLE_ID','1','=')
	->process( $_POST )
	->json();