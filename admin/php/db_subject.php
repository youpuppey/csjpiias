<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'tbl_subject', 'SUB_ID' )
	->fields(
		Field::inst( 'SUB_ID' ),
        Field::inst( 'SUB_CODE' ),
        Field::inst( 'SUB_TITLE' ),
        Field::inst( 'SUB_STATUS' )
        
	)
	->process( $_POST )
	->json();