<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'tbl_info', 'INFO_ID' )
	->fields(
		Field::inst( 'INFO_ID' ),
        Field::inst( 'INFO_NUM' ),
        Field::inst( 'INFO_LNAME' ),
        Field::inst( 'INFO_FNAME' ),
        Field::inst( 'INFO_MNAME' ),
        Field::inst( 'INFO_STATUS' )
        
	)
    ->where('ROLE_ID','2','=')
	->process( $_POST )
	->json();