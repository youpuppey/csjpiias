<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title">Students</h4>
                          </div>
                          <div class="card-block">
                          <div class="row mr-0">
                             <div class="col-md-6 offset-md-6 mb-1">
                                 <div class="row">
                                      <div class="col-md-6 px-0" style="text-align:center">
                                            <label for="searchbycourse" class="px-0 pl-3">Search student by course</label>
                                      </div>
                                      <div class="col-md-6 ">
                                        <select class="form-control pr-3 mr-3" id="searchbycourse" onchange="searchbycourse(this.value);">
                                          <option value="0">All</option>
                                          <?php
                                            require('../connect.php');
                                            $query="SELECT * FROM tbl_course";
                                            $result=$conn->query($query);
                                            while($row=$result->fetch_object()){
                                                $courseid=$row->COURSE_ID;
                                                $coursecode=$row->COURSE_CODE;
                                                ?>
                                                <option value="<?=$courseid;?>"><?=$coursecode;?></option>
                                                <?php
                                            }

                                            ?>
                                        </select>
                                      </div>
                                 </div>
                             </div>
                          </div>
                           
                            <table id="studentTable"  class="table table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Student Number</th>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Course</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <?php include('datatablesScript.php');?>
    <script>
        $(document).ready(function(){
            loadTable();
        });
        
        function loadTable(){
    
            $('#studentTable').DataTable( {
                "bLengthChange": false,
                "pageLength": 5,
                "pagingType": "full",
                responsive: true,
                ajax: "php/db_student.php",
                order: [[ 0, 'asc' ]],
                columns: [
                    { data: "tbl_info.INFO_NUM" },
                    { data: "tbl_info.INFO_LNAME" },
                    { data: "tbl_info.INFO_FNAME" },
                    { data: "tbl_course.COURSE_CODE" },
                    { data: "tbl_info.INFO_STATUS" },
                    { data: "tbl_info.INFO_ID","render":function ( data, type, full, meta ) {
                    return '<a href="view_student.php?id='+data+'">View</a>';}}
                ]
            } );
            
        }
    </script>
</body>
</html>