<html>
    <head>
        <link rel="stylesheet" href="../datatables/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="../datatables/extensions/Buttons/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="../datatables/extensions/Select/css/select.dataTables.min.css">
        <link rel="stylesheet" href="../datatables/extensions/Editor/css/dataTables.editor.css">
        <script src="../js/jquery-2.1.1.min.js"></script>
        <script src="../datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="../datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
        <script src="../datatables/extensions/Select/js/dataTables.select.min.js"></script>
        <script src="../datatables/extensions/Editor/js/dataTables.editor.min.js"></script>
    </head>
    <body>
        <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Employee Num</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Middle Name</th>
                <th>Contact #</th>
                <th width="18%">BDAY</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    <script>
        var editor; // use a global for the submit and return data rendering in the examples
 
$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "php/staff.php",
        table: "#example",
        fields: [ {
                
                name: "INFO_ID"
            }, {
                
                name: "INFO_NUM"
            }, {
                
                name: "INFO_FNAME"
            }, {
                
                name: "INFO_MNAME"
            }, {
                
                name: "INFO_LNAME"
            }, {
                
                name: "INFO_CONTACT_NUM"
            }, {
                
                name: "INFO_BDAY"
            }, {
               
                name: "INFO_GENDER"
            }
        ]
    } );
 
    // Activate an inline edit on click of a table cell
    $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
    } );
 
    $('#example').DataTable( {
        dom: "Bfrtip",
        ajax: "php/staff.php",
        order: [[ 0, 'asc' ]],
        columns: [
            { data: "INFO_ID" },
            { data: "INFO_NUM" },
            { data: "INFO_FNAME" },
            { data: "INFO_MNAME" },
            { data: "INFO_LNAME" },
            { data: "INFO_CONTACT_NUM" },
            { data: "INFO_BDAY" },
            { data: "INFO_GENDER"},
            { data: "INFO_ID","render":function ( data, type, full, meta ) {
   return '<a href="manage_faculty.php?id='+data+'">Edit</a>';}}
        ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ]
    } );
} );
    </script>
    </body>
</html>