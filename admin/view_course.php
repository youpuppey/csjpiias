<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../css/admin.css">
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                      <?php
                        require_once('../connect.php');
                        $coursecode=$coursedesc=$coursedate=$coursestatus=$contact="N/A";
                        if(isset($_GET['id'])){
                            $id=$_GET['id'];
                            $sql="SELECT * FROM tbl_course WHERE COURSE_ID='$id'";
                            $result = $conn->query($sql);
                            $row=$result->fetch_object();
                            $coursecode=$row->COURSE_CODE;
                            $coursedate=$row->COURSE_DATE;
                            $coursedesc=$row->COURSE_DESCRIPTION;
                            $coursestatus=$row->COURSE_STATUS;
                        }
                        ?>
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title"><?=$coursedesc;?> <a href="edit_course.php?id=<?=$_GET['id'];?>" class="btn btn-warning pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> EDIT</a></h4>
                          </div>
                          <div class="card-block">
                            <div class="facultyInfo">
                                <div class="row">
                                    <div class="col-12">
                                        <span style="font-weight:800;">Course Details</span>
                                        <hr>
                                    </div>
                                    
                                    <div class="col-md-4">Course Code: <i class="facultyData"><?=$coursecode;?></i></div>
                                    <div class="col-md-8">Course Description: <i class="facultyData"><?=$coursedesc;?></i></div>
                                    <div class="col-md-4">Date Created: <i class="facultyData"><?=$coursedate;?></i></div>
                                    <div class="col-md-8">Status: <i class="facultyData"><?=$coursestatus;?></i></div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12"><button>Deactivate</button></div>
                                </div>    
                            </div>
                          </div>
                        </div>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
        
</body>
</html>