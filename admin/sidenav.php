<div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                   <center><img src="../img/logo.png" width="47" height="47" alt="CSJPIIAS"></center>
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-home"></i> Home</a>
                </li>
                <li>
                    <a href="group_list.php"><i class="fa fa-group"></i> Groups</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-book"></i> Quiz</a>
                </li>
                <li>    
                    <a href="#"><i class="fa fa-area-chart"></i> Grade</a>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#admin_nav"><i class="fa fa-user-secret"></i> Admin <i class="fa fa-caret-down"></i></a>
                    <ul id="admin_nav" class="collapse">
                        <li>
                            <a href="manage_faculty.php"><i class="fa" style="color:#00e5ff;">&#9900;</i> Manage Faculty</a>
                        </li>
                        <li>
                            <a href="manage_student.php"><i class="fa" style="color:#00e676;">&#9900;</i> Manage Students</a>
                        </li>
                        <li>
                            <a href="manage_user.php"><i class="fa" style="color:#ecf0f1;">&#9900;</i> Manage User</a>
                        </li>
                        <li>
                            <a href="manage_course.php"><i class="fa" style="color:#ea80fc;">&#9900;</i> Manage Course</a>
                        </li>
                        <li>
                            <a href="manage_semester.php"><i class="fa" style="color:#00897b;">&#9900;</i> Manage Semester</a>
                        </li>
                        <li>
                            <a href="manage_acadyear.php"><i class="fa" style="color:#aa00ff;">&#9900;</i> Manage A-Y</a>
                        </li>
                        <li>
                            <a href="manage_subject.php"><i class="fa" style="color:#eeff41;">&#9900;</i> Manage Subjects</a>
                        </li>
                        <li>
                            <a href="manage_quiz.php"><i class="fa" style="color:#16a085;">&#9900;</i> Manage Quiz</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa" style="color:#f44336;">&#9900;</i> Audit Trail</a>
                        </li>
                    </ul>
                </li>
                <li><hr></li>
            </ul>
        </div>