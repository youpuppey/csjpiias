<!DOCTYPE html>
<html>
<head>
   <!-- Head settings -->
    <?php include('head.php');?>
    <link rel="stylesheet" href="../css/admin.css">
    <?php include('datatablesCss.php');?>
</head>
<body>
    <!-- Navbar -->
    <?php include('navbar.php');?>
    <div id="wrapper">
        <?php include('sidenav.php');?>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                   <!--Left side-->
                    <?php include('leftColumn.php');?>
                    <!--Middle-->
                    <div class="col-lg-8 col-sm-12" style="padding:0;">
                      <?php
                        require_once('../connect.php');
                        $fname=$lname=$mname=$address=$contact=$gender=$email=$bday=$gname=$gcontact=$infonum=$type=$selectGender="N/A";
                        if(isset($_GET['id'])){
                            $id=$_GET['id'];
                            $sql="SELECT * FROM tbl_info WHERE INFO_ID='$id'";
                            $result = $conn->query($sql);
                            $row=$result->fetch_object();
                            $fname=$row->INFO_FNAME;
                            $mname=$row->INFO_MNAME;
                            $lname=$row->INFO_LNAME;
                            $address=$row->INFO_ADDRESS;
                            $email=$row->INFO_EMAIL;
                            $gender=$row->INFO_GENDER;
                            $bday=$row->INFO_BDAY;
                            $contact=$row->INFO_CONTACT_NUM;
                            $gname=$row->INFO_GUARDIAN_NAME;
                            $gcontact=$row->INFO_GUARDIAN_NUMBER;
                            $infonum=$row->INFO_NUM;
                            $type=$row->ROLE_ID;
                            
                            if($gender=="MALE"){
                                $selectGender="
                                    <option value='MALE' selected>MALE</option>
                                    <option value='FEMALE'>FEMALE</option>
                                ";
                                }
                            if($gender=="FEMALE"){
                                $selectGender="
                                    <option value='FEMALE' selected>FEMALE</option>
                                    <option value='MALE'>MALE</option>
                                ";
                                }
                                
                        }
                        ?>
                        <form id="updateFormFaculty">
                        <input type="hidden" value="<?=$_GET['id'];?>" id="uid">
                       <div class="card" style="max-width:100%;overflow-x:auto;padding:0;">
                          <div class="card-header">
                            <h4 class="card-title">EDIT INFORMATION<a href="view_faculty.php?id=<?=$_GET['id'];?>" class="btn btn-danger pull-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> CANCEL</a></h4>
                          </div>
                          <div class="card-block">
                           <div class="facultyInfo">
                            <ul class="nav nav-tabs" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#employment" role="tab">Employment</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#contactinfo" role="tab">Contact</a>
                              </li>
                            </ul>
                            
                            <div class="tab-content">
                             
                              <!--Personal Tab-->
                              <div class="tab-pane active" id="personal" role="tabpanel">
                                  <div class="row">
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="fname" class="col-sm-2 col-12 col-form-label">First Name</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$fname;?>" id="fname">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="mname" class="col-sm-2 col-12 col-form-label">Middle Name</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$mname;?>" id="mname">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="lname" class="col-sm-2 col-12 col-form-label">Last Name</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$lname;?>" id="lname">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="bday" class="col-sm-2 col-12 col-form-label">Birth date (mm-dd-yyyy)</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="date" value="<?=$bday;?>" id="bday">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="gender" class="col-sm-2 col-12 col-form-label">Gender</label>
                                          <div class="col-sm-10 col-12">
                                            <select class="form-control" id="gender">
                                                <?=$selectGender;?>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                              
                              <!--Employment tab-->
                              <div class="tab-pane" id="employment" role="tabpanel">
                                  <div class="row">
                                      <div class="col-md-6 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="infonum" class="col-sm-2 col-12 col-form-label">Employee ID</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$infonum;?>" id="infonum">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="type" class="col-sm-2 col-12 col-form-label">Employee Type</label>
                                          <div class="col-sm-10 col-12">
                                            <select class="form-control" id="type">
                                                <option value="<?=$type;?>" <?php echo ($type==1 ? "selected" : "" ); ?> >STUDENT</option>
                                                <option value="2" <?php echo ($type==2 ? "selected" : "" ); ?> >TEACHER</option>
                                                <option value="3" <?php echo ($type==3 ? "selected" : "" ); ?> >DEAN</option>
                                                <option value="4" <?php echo ($type==4 ? "selected" : "" ); ?> >ADMIN</option>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="tab-pane" id="contactinfo" role="tabpanel">
                                  <div class="row">
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="address" class="col-sm-2 col-12 col-form-label">Address</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$address;?>" id="address">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="contact" class="col-sm-2 col-12 col-form-label">Contact</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$contact;?>" id="contact">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12 px-1 py-1">
                                         <div class="form-group row">
                                          <label for="email" class="col-sm-2 col-12 col-form-label">Email</label>
                                          <div class="col-sm-10 col-12">
                                            <input class="form-control" type="text" value="<?=$email;?>" id="email">
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                              <a href="javascript:void(0);" id="updateInfo" class="btn btn-success mx-auto"><i class="fa fa-floppy-o" aria-hidden="true"></i> SAVE <img src="../img/loading.gif" width="10px" height="10px" id="submitloader" alt="loading" /></a>
                            </div>
                           </div>
                          </div>
                        </div>
                        </form>
                    </div>
                    <!--Right side-->
                    <?php include('rightColumn.php');?>
                </div>
            </div>
        </div>
    </div>
    <!-- Script settings -->
    <?php include('scripts.php');?>
    <script>
        $("document").ready(function(){
            $("#submitloader").hide();
        });
    </script>
        
</body>
</html>