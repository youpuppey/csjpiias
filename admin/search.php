<?php
require_once("../connect.php");
$type=$conn->real_escape_string($_GET['type']);
$value=$conn->real_escape_string($_GET['value']);
if($type=="course"){
    if($value>0){
        $query="SELECT * FROM tbl_info as i, tbl_course as c WHERE i.COURSE_ID=c.COURSE_ID AND i.COURSE_ID='$value' AND i.ROLE_ID=1";
        $result=$conn->query($query);
        ?>
            <thead>
                <tr>
                    <th>Student Number</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Course</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
        <?php
        while($row=$result->fetch_object()){
        ?>
        <tr>
                    <td><?=$row->INFO_NUM;?></td>
                    <td><?=$row->INFO_LNAME;?></td>
                    <td><?=$row->INFO_FNAME;?></td>
                    <td><?=$row->COURSE_CODE;?></td>
                    <td><?=$row->INFO_STATUS;?></td>
                    <td><a href="view_student.php?id=<?=$row->INFO_ID;?>">View</a></td>
                </tr>
        <?php
        }
        ?>
    </tbody>
        <?php
    }else{
        ?>
        <?php
    }
}
?>