<?php
$ajax = ($_SERVER[ 'HTTP_X_REQUESTED_WITH' ] === 'XMLHttpRequest');
session_start();

$added = add_signin($_POST);

function add_signin($vars) {
    
    $added = false;
    
    $input = array(
     'username' => $vars['username'],
     'password' => $vars['password'],
     'date' => date('y/m/d H:i:s'));
    
      $added = checkSignin($input);

      //$this->post_to_db($added);
      
    
    return $added;
  }
function checkSignin($input){
    require_once('connect.php');
    
    $added=false;
    $attempt=0;
    if(isset($_COOKIE['attempt'])){
        $attempt=$_COOKIE['attempt'];
    }
    $user=$conn->real_escape_string($input['username']);
    $pass=$conn->real_escape_string(sha1($input['password']));
    $date=$conn->real_escape_string($input['date']);
    
    if($attempt<=5){
        $query="SELECT * FROM tbl_user WHERE USER_USER='$user' AND USER_PASS='$pass'";
        $result=$conn->query($query);
        $row=$result->fetch_object();
        $count=$result->num_rows;
        if($count>0){
            $info_id=$row->INFO_ID;
            $user_id=$row->USER_ID;
            $query="SELECT * FROM tbl_info WHERE INFO_ID='$info_id'";
            $result=$conn->query($query);
            $row=$result->fetch_object();
            $_SESSION['INFO_ID']=$info_id;
            $_SESSION['USER_ID']=$user_id;
            $_SESSION['ROLE_ID']=$row->ROLE_ID;
            $_SESSION['INFO_FNAME']=$row->INFO_FNAME;
            $_SESSION['INFO_LNAME']=$row->INFO_LNAME;
            $_SESSION['INFO_MNAME']=$row->INFO_MNAME;
            $_SESSION['INFO_NUM']=$row->INFO_NUM;
            $_SESSION['INFO_PICTURE']=$row->INFO_PROFILE_PICTURE;
            $input['role']=$row->ROLE_ID;
            $input['response']="success";
            $added = $input;
            
        }else{
            $attempt+=1;
            setcookie("attempt", $attempt, time() + (60 * 5));
            $input['attempt']=$attempt;
        }
    }else{
        $input['response']="fail";
        $input['attempt']=$attempt;
        $added=$input;
    }
    return $added;
}

if($ajax) {
  sendAjaxResponse($added);
}
else {
  sendStandardResponse($added); 
}

function sendAjaxResponse($added) {
  header("Content-Type: application/json");
  if($added) {
    header( 'Status: 201' );
    echo( json_encode($added) );
  }
  else {
    header( 'Status: 400' );
  }
}

function sendStandardResponse($added) {
  if($added) {
      header( 'Location: index.php' );
  }
  else {
    header( 'Location: index.php?error=No response from the server' );
  }
}
    
?>