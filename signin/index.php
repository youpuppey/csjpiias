<!DOCTYPE html>
<?php
 session_start();
include("headerLink.php");
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CSJPIIAS E-LEARNING" />
    <meta name="author" content="KENNETH CAMBAYA" />
    <link rel="icon" type="image/png" href="../img/favicon1.png" >
    <link rel="stylesheet" href="../fontawesome/css/font-awesome.min.css">

    <title>College of Saint John Paul II Arts and Sciences</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/cover.css" rel="stylesheet">
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">
          
          <div class="inner cover">
              <img src="../img/logo.png" alt="CSJPII" height="140px" width="140px">
           <form id="signinForm" class="form-signin" action="../login.php"  method="POST">
                <h1 class="cover-heading">Sign in </h1>
                <br />
                <div class="alert alert-danger" role="alert" id="alertMsg">
                  <strong>Incorrect Username or Password</strong> try again. <a href="javascript:void(0);" onclick="hideAlert();" class="close">x</a>
                </div>
                <div class="alert alert-danger" role="alert" id="alertLimit">
                  <strong>Excessive Log in attempts</strong> try again after 5mins. <a href="javascript:void(0);" onclick="hideAlert();" class="close">x</a>
                </div>
                <div class="input-group">
                <!--<label for="inputUsername" class="sr-only">Email address</label>-->
                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                <input style="border:1px solid #eceeef" type="text" id="inputUsername" class="form-control" placeholder="Username" aria-describedby="basic-addon1" autocorrect="off" autocapitalize="none" spellcheck="false" required autofocus pattern=".{5,}" title="atleast 5 characters">
                </div>
                <br />
                <div class="input-group">
                <!--<label for="inputPassword" class="sr-only">Password</label>-->
                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-key fa-fw" aria-hidden="true"></i></span>
                <input style="border:1px solid #eceeef" type="password" id="inputPassword" class="form-control" placeholder="Password" aria-describedby="basic-addon2" required>
                </div>
                <br />
                <button class="btn btn-lg btn-primary btn-block" type="submit"><span id="signinIcon">Sign in <i class="fa fa-sign-in" aria-hidden="true"></i></span><img src="../img/loading.gif" height="30px" id="signinLoader" width="30px" alt="loading"></button>
                <br />
                <a href="../" class="back">Back to home</a>
            </form>
          </div>
          <div class="mastfoot">
            <div class="inner">
              <p>&copy; 2017 CSJPII.ESY.ES  | <a href="http://www.facebook.com/youpuppey" target="_blank">by Kenneth A. Cambaya</a></p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/access.js" type="text/javascript"></script>
  </body>
</html>
