<?php
if(isset($_SESSION['USER_ID'])){
    if($_SESSION['ROLE_ID']==1){
        header("Location: ../student/");
    }elseif($_SESSION['ROLE_ID']==2){
        header("Location: ../faculty/");
    }elseif($_SESSION['ROLE_ID']==3){
        header("Location: ../manager/");
    }elseif($_SESSION['ROLE_ID']==4){
        header("Location: ../admin/");
    }
}
?>