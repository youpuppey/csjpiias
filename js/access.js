$(document).ready(function() {
    $("#signinLoader").hide();
    $("#alertMsg").hide();
    $("#alertLimit").hide();
});
$(function() {
  $('#signinForm').submit(handleSignin);
});
function hideAlert() {
    $("#alertMsg").fadeOut();
    $("#alertLimit").fadeOut();
}

function handleSignin() {
  var form = $(this);
  var data = {
    "username": form.find('#inputUsername').val(),
    "password": form.find('#inputPassword').val()
  };

  postSignin(data);
  return false;

}
function postSignin(data) {
    $.ajax({
    type: 'POST',
    url: '../login.php',
    data: data,
    timeout: 15000,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    beforeSend: signinProcess,
    success: signinSuccess,
    error: signinError
  });
}
function signinSuccess(data, textStatus, jqXHR) {
  console.log("Success");
    if(data.response=="success"){
        if(data.role==1){
             window.location.href="../student";
        }else if(data.role==2){
             window.location.href="../faculty";
        }else if(data.role==3){
             window.location.href="../manager";
        }else{
             window.location.href="../admin";
        }   
    }else if(data.attempt>5){
        showLimit();
        removeLoader();
    }else{
        signinError();
    }
    
   
}

function signinError(jqXHR, textStatus, errorThrown) {
  removeLoader();
  console.log("Error");
  showError();
    
  //console.debug(jqXHR);
}
function signinProcess() {
    $("#signinIcon").hide();
    $("#signinLoader").fadeIn(100); 
    console.log("Processing. . .");
}
function removeLoader() {
    $("#signinLoader").fadeOut(1000);
    setTimeout(function(){ $("#signinIcon").fadeIn(100); }, 1000);
}
function showError(){
    setTimeout(function(){ $("#alertMsg").fadeIn(200); }, 1000);
}
function showLimit(){
    $("#alertMsg").hide();
    setTimeout(function(){ $("#alertLimit").fadeIn(200); }, 1000);
}