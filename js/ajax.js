$("#updateInfo").on("click",function() {
    var dataString = {
        "uid": $("#updateFormFaculty").find("#uid").val(),
        "fname": $("#updateFormFaculty").find("#fname").val(),
        "mname": $("#updateFormFaculty").find("#mname").val(),
        "lname": $("#updateFormFaculty").find("#lname").val(),
        "bday": $("#updateFormFaculty").find("#bday").val(),
        "gender": $("#updateFormFaculty").find("#gender").val(),
        "infonum": $("#updateFormFaculty").find("#infonum").val(),
        "type": $("#updateFormFaculty").find("#type").val(),
        "address": $("#updateFormFaculty").find("#address").val(),
        "contact": $("#updateFormFaculty").find("#contact").val(),
        "email": $("#updateFormFaculty").find("#email").val()
    };
    
    var url = "../admin/updateFaculty.php"; //URL to send data
    
    $.ajax({
    type: 'POST',
    url: url,
    data: dataString,
    timeout: 15000,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    beforeSend: updateProcess,
    success: updateSuccess,
    error: updateError
  });
});

$("#updateStudentInfo").on("click",function() {
    var dataString = {
        "uid": $("#updateFormStudent").find("#uid").val(),
        "fname": $("#updateFormStudent").find("#fname").val(),
        "mname": $("#updateFormStudent").find("#mname").val(),
        "lname": $("#updateFormStudent").find("#lname").val(),
        "bday": $("#updateFormStudent").find("#bday").val(),
        "gender": $("#updateFormStudent").find("#gender").val(),
        "infonum": $("#updateFormStudent").find("#infonum").val(),
        "type": $("#updateFormStudent").find("#type").val(),
        "address": $("#updateFormStudent").find("#address").val(),
        "contact": $("#updateFormStudent").find("#contact").val(),
        "course": $("#updateFormStudent").find("#course").val(),
        "email": $("#updateFormStudent").find("#email").val()
    };
    
    var url = "../admin/updateStudent.php"; //URL to send data
    
    $.ajax({
    type: 'POST',
    url: url,
    data: dataString,
    timeout: 15000,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    beforeSend: updateProcess,
    success: updateStudentSuccess,
    error: updateError
  });
});


function searchbycourse(courseid){
    
   if ( $.fn.DataTable.isDataTable('#studentTable') ) {
      $('#studentTable').DataTable().destroy();
    }
    if(courseid > 0){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange= function(){
                if(xmlhttp.readyState==4 && xmlhttp.status==200){
                    $('#studentTable').html(xmlhttp.responseText);
                    $('#studentTable').DataTable( {
                        "bLengthChange": false,
                        "pageLength": 5,
                        "pagingType": "full",
                        responsive: true,
                        order: [[ 0, 'asc' ]]
                    } );
                }
            };
            xmlhttp.open("GET","search.php?type=course&&value="+ courseid,true);
            xmlhttp.send();
       }else{
           loadTable();
       }
}










function updateSuccess(data, textStatus, jqXHR) {
  console.log(data);
    if(data.response=="success"){
        removeLoader();
        alert("Successfully updated record!");
        window.location.href="view_faculty.php?id="+$("#uid").val();
    }else{
        updateError();
    }
    
   
}
function updateStudentSuccess(data, textStatus, jqXHR) {
  console.log(data);
    if(data.response=="success"){
        removeLoader();
        alert("Successfully updated record!");
        window.location.href="view_student.php?id="+$("#uid").val();
    }else{
        updateError();
    }
    
   
}

function updateError(jqXHR, textStatus, errorThrown) {
  removeLoader();
  alert("Error occured! Please contact administrator.");
}
function removeLoader() {
    $("#submitloader").fadeOut(1000);
    setTimeout(function(){ $("#submiticon").fadeIn(100); }, 1000);
}
function updateProcess() {
    $("#submitloader").fadeIn(100); 
    console.log("Processing. . .");
}