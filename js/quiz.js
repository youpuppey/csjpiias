var type=1;
var uid = document.getElementById("uid").value;
var qid = document.getElementById("qid").value;
$(document).ready(function() {
    $("#controlList").hide();
    $("#controlForm").hide();
    if(localStorage.autosave){
        saved = JSON.parse(localStorage.getItem('autosave'));
        if(saved[0]==uid){
            var length = saved[1].length;
            if(length>0){
                $("#controlButtons").hide();
                $("#controlList").show();
                $("#controlForm").show();
                for(var x =0; x < length ; x++){
                    var type=parseInt(saved[1][x].type);
                    if(type==1){
                       $("#listItems").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item'+ (x+1) +'" role="tab">'+ (x+1) +'</a></li>');
                       typeElement="<option value='1' selected>Multiple Choice</option><option value='2'>True or False</option><option value='3'>Fill in the blank</option>";
                       $("#formPanel").append('<div class="tab-pane" id="item'+ (x+1) +'" role="tabpanel"> <div class="row"> <div class="col-12"> <span class="quizText">Type:</span> <select class="quizType" onchange="changeType(this.value,'+ (x+1) +')">'+ typeElement +'</select> <span class="quizText">Points:</span> <input type="number" min="0" name="points" class="quizPoints" onblur="checkEmptyPoints(this)" oninput="updatePoints();" /> <button type="button" class="pull-right btn-danger deleteItem deleteBtn" onclick="deleteItem(\'item'+ (x+1) +'\')">Delete</button> </div><br/> <br/> <div class="col-12 typeForm" id="typeForm'+ (x+1) +'"> <div class="form-group"> </div></div></div>');
                        var q=saved[1][x].question;
                        var a=saved[1][x].option1;
                        var b=saved[1][x].option2;
                        var c=saved[1][x].option3;
                        var d=saved[1][x].option4;
                        var p=saved[1][x].points;
                        $("#item"+(x+1)).find("input[name=points]").val(parseInt(p));
                        changeTypeMC(1,x+1,q,a,b,c,d);
                        updateItems();
                        updatePoints();
                    }
                    else if(type==2){
                        $("#listItems").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item'+ (x+1) +'" role="tab">'+ (x+1) +'</a></li>');
                        typeElement="<option value='1'>Multiple Choice</option><option value='2' selected>True or False</option><option value='3'>Fill in the blank</option>";
                        $("#formPanel").append('<div class="tab-pane" id="item'+ (x+1) +'" role="tabpanel"> <div class="row"> <div class="col-12"> <span class="quizText">Type:</span> <select class="quizType" onchange="changeType(this.value,'+ (x+1) +')">'+ typeElement +'</select> <span class="quizText">Points:</span> <input type="number" min="0" name="points" class="quizPoints" onblur="checkEmptyPoints(this)" oninput="updatePoints();" /> <button type="button" class="pull-right btn-danger deleteItem deleteBtn" onclick="deleteItem(\'item'+ (x+1) +'\')">Delete</button> </div><br/> <br/> <div class="col-12 typeForm" id="typeForm'+ (x+1) +'"> <div class="form-group"> </div></div></div>');
                        var q=saved[1][x].question;
                        var a=saved[1][x].option1;
                        var p=saved[1][x].points;
                        $("#item"+(x+1)).find("input[name=points]").val(parseInt(p));
                        changeTypeTF(2,x+1,q,a);
                        updateItems();
                        updatePoints();
                    }else{
                        $("#listItems").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item'+ (x+1) +'" role="tab">'+ (x+1) +'</a></li>');
                        typeElement="<option value='1'>Multiple Choice</option><option value='2'>True or False</option><option value='3' selected>Fill in the blank</option>";
                        $("#formPanel").append('<div class="tab-pane" id="item'+ (x+1) +'" role="tabpanel"> <div class="row"> <div class="col-12"> <span class="quizText">Type:</span> <select class="quizType" onchange="changeType(this.value,'+ (x+1) +')">'+ typeElement +'</select> <span class="quizText">Points:</span> <input type="number" min="0" name="points" class="quizPoints" onblur="checkEmptyPoints(this)" oninput="updatePoints();" /> <button type="button" class="pull-right btn-danger deleteItem deleteBtn" onclick="deleteItem(\'item'+ (x+1) +'\')">Delete</button> </div><br/> <br/> <div class="col-12 typeForm" id="typeForm'+ (x+1) +'"> <div class="form-group"> </div></div></div>');
                        var q=saved[1][x].question;
                        var a=saved[1][x].option1;
                        var p=saved[1][x].points;
                        $("#item"+(x+1)).find("input[name=points]").val(parseInt(p));
                        changeTypeFIB(3,x+1,q,a);
                        $("#item"+(x+1)).find("input[name=option1]").val(a);
                        updateItems();
                        updatePoints();
                    }

                }
            }
            else{
                localStorage.removeItem('autosave');
            }
            
        }
        else{
            localStorage.removeItem('autosave');
        }
    }
});

$("#addFirstBtn").click(function(){
    $("#controlButtons").hide();
    $("#controlList").show();
    $("#controlForm").show();
    $("#addItem").click();
});

$("#saveQuiz").click(function(){
    checkAllEmpty();
    arraySave();
    if(empty==0){
         localStorage.removeItem('autosave');
        var li = $("ul#listItems li").length;
        for(var x =0;x<li;x++){
            var id = items[0];
            var quizid=qid;
            var type=items[1][x].type;
            if(type==1){
                    var q=items[1][x].question;
                    var a=items[1][x].option1;
                    var b=items[1][x].option2;
                    var c=items[1][x].option3;
                    var d=items[1][x].option4;
                    var points=items[1][x].points;
                    storeDB(quizid,type,points,q,a,b,c,d);
               }
            else if(type==2){
                    var q=items[1][x].question;
                    var a=items[1][x].option1;
                    var points=items[1][x].points;
                    storeDB(quizid,type,points,q,a);
               }
            else if(type==3){
                    var q=items[1][x].question;
                    var a=items[1][x].option1;
                    var points=items[1][x].points;
                    storeDB(quizid,type,points,q,a);
               }
            else{
                    alert("Error determining type! Please contact administrator.")
                }
        }
        alert("Quiz successfully added!");
        window.location.href="manage_quiz.php";
    }else{
        alert("Please Complete Form!");
    }
});

function storeDB(qid,type,p,q,a,b='',c='',d=''){
    var xhttp=new XMLHttpRequest();
	xhttp.onreadystatechange=function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			
		}
	};
	xhttp.open("GET","add_items.php?qid="+qid+"&type="+type+"&q="+q+"&a="+a+"&b="+b+"&c="+c+"&d="+d+"&p="+p,true);
	xhttp.send();
}


$("#addItem").click(function(){
    var typeElement="";
    var li = $("ul#listItems li").length;
    $("#listItems").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item'+ (li+1) +'" role="tab">'+ (li+1) +'</a></li>');
    if(type==1){
        typeElement="<option value='1' selected>Multiple Choice</option><option value='2'>True or False</option><option value='3'>Fill in the blank</option>";
    }if(type==2){
        typeElement="<option value='1'>Multiple Choice</option><option value='2' selected>True or False</option><option value='3'>Fill in the blank</option>";
    }if(type==3){
        typeElement="<option value='1'>Multiple Choice</option><option value='2'>True or False</option><option value='3' selected>Fill in the blank</option>";
    }
    $("#formPanel").append('<div class="tab-pane show active" id="item'+ (li+1) +'" role="tabpanel"> <div class="row"> <div class="col-12"> <span class="quizText">Type:</span> <select class="quizType" onchange="changeType(this.value,'+ (li+1) +')">'+ typeElement +'</select> <span class="quizText">Points:</span> <input type="number" min="0" name="points" class="quizPoints" onblur="checkEmptyPoints(this)" oninput="updatePoints();" /> <button type="button" class="pull-right btn-danger deleteItem deleteBtn" onclick="deleteItem(\'item'+ (li+1) +'\')">Delete</button> </div><br/> <br/> <div class="col-12 typeForm" id="typeForm'+ (li+1) +'"> <div class="form-group"> </div></div></div>');
    changeType(type,(li+1));
    $('#listItems a:last').tab('show');
    updateItems();
    closeItem();
});
$("textarea").keyup(function(){
    this.style.overflow = 'hidden';
    this.style.height = 0;
    this.style.height = this.scrollHeight + 'px';
});


function loopItems(){
    var li = $("ul#listItems li").length;
    
    $("#listItems").html("");
    for(var x=1;x<=li;x++){
        $("#listItems").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item'+ x +'" role="tab">'+ x +'</a></li>');    
    }
    var x=1;
    $("#formPanel").find('[id*="item"]').each(function(){
            // Update the 'rules[0]' part of the name attribute to contain the latest count
            $("#"+$(this).attr('id')).attr('id','item'+x);
            $("#"+$(this).attr('id')+">div>div>.deleteItem").attr('onclick','deleteItem(\"item'+x+'\")');
            $("#"+$(this).attr('id')+">div>.typeForm").attr('id','typeForm'+x);
        
            x++;
        }); 
}

function deleteItem(item){
    console.log("deleteActivate");
    $('a[href$="'+ item +'"]').parent().remove();
    $('#'+item).remove();
    arraySave();
    var li = $("ul#listItems li").length;
    if(li>0){
        loopItems();
        closeItem();
        updatePoints();
        updateItems();
        checkAllEmpty();
       }else{
           $("#controlButtons").show();
           $("#controlList").hide();
           $("#controlForm").hide();
       }
}

function closeItem(){
    $('li > a[href*="item"]').hover(function(){
        /*alert($(this).attr("href").substring(1));*/
        $("#close"+$(this).attr("href").substring(1)).show();
    }).mouseleave(function(){
        /*alert($(this).attr("href").substring(1));*/
        $("#close"+$(this).attr("href").substring(1)).hide();
    });
}

function fillText(id,value){
    value = value.replace("_", "<input type='text' class='fillAns' id='fillAns"+ id +"' name='option1'/>");
    value = value.replace(/  /g, "&nbsp;&nbsp;");
    value = value.replace(/\n/g, "<br />");
    $("#fill"+id).html(value);
}

function changeType( select, id ){
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange= function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
	       type=select;
            document.getElementById('typeForm'+id).innerHTML=xmlhttp.responseText;
		}
	};
		
	xmlhttp.open("GET",'changeType.php?select='+select+"&id="+id,true);
	xmlhttp.send();
}
function changeTypeMC(select,id,q,a,b,c,d ){
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange= function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
	       type=select;
            document.getElementById('typeForm'+id).innerHTML=xmlhttp.responseText;
		}
	};
		
	xmlhttp.open("GET",'changeType.php?select='+select+"&id="+id+"&question="+q+"&option1="+a+"&option2="+b+"&option3="+c+"&option4="+d,true);
	xmlhttp.send();
}
function changeTypeTF(select,id,q,a ){
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange= function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
	       type=select;
            document.getElementById('typeForm'+id).innerHTML=xmlhttp.responseText;
		}
	};
		
	xmlhttp.open("GET",'changeType.php?select='+select+"&id="+id+"&question="+q+"&option1="+a,true);
	xmlhttp.send();
}
function changeTypeFIB(select,id,q,a ){
	
	$("#typeForm"+id).html('<span class="quizText">Note: Use \' _ \' to specify where you would like a blank to appear in the text below. Then enter the correct answer in the textbox.</span> <div class="form-group"> <label>Question:</label> <textarea class="form-control quizTextarea" onkeyup="fillText(\''+ id +'\',this.value)" name="question">'+ q +'</textarea> </div><p id="fill'+ id +'"></p>');
     fillText(id,q);
}
    
var items=[];
function arraySave(){
    items=[uid,[]];
    var x=1;
        $("#formPanel").find('.quizType').each(function(){
            if($(this).val()==1){
                 var q = $("#typeForm"+x).find("textarea[name=question]").val();
                    var a = $("#typeForm"+x).find("input[name=option1]").val();
                    var b = $("#typeForm"+x).find("input[name=option2]").val();
                    var c = $("#typeForm"+x).find("input[name=option3]").val();
                    var d = $("#typeForm"+x).find("input[name=option4]").val();
                    var p = $("#item"+x).find("input[name=points]").val();
                    var form = {type:1,question:q,option1:a,option2:b,option3:c,option4:d,points:p};
                    items[1].push(form);
                    console.log("pushmc");
                    x++;
            }
            else if($(this).val()==2){
                    var q = $("#typeForm"+x).find("textarea[name=question]").val();
                    var a = $("#typeForm"+x).find("select[name=option1]").val();
                    var p = $("#item"+x).find("input[name=points]").val();
                    var form = {type:2,question:q,option1:a,points:p};
                    items[1].push(form);
                    console.log("pushtf");
                    x++;
            }
            else if($(this).val()==3){
                    var q = $("#typeForm"+x).find("textarea[name=question]").val();
                    var a = $("#typeForm"+x).find("input[name=option1]").val();
                    var p = $("#item"+x).find("input[name=points]").val();
                    var form = {type:3,question:q,option1:a,points:p};
                    items[1].push(form);
                    console.log("fib");
                    x++;
            }
            else{
                alert("Error gathering data! Name attributes changed!");
            }
        });

    saveStorage();
}
function updatePoints(){
    var totalPoints = 0;
    $("input[name=points]").each(function(){

    if(parseInt($(this).val())>0)
    totalPoints+=parseInt($(this).val());

    $("#tpoints").html(totalPoints);

    });
}

function updateItems(){
    var li = $("ul#listItems li").length;
    $("#titems").html(li);

}
var empty=0;
function checkAllEmpty(){
    
    empty=0;
    $("#formPanel").find('[id*="item"]').each(function(){
        var id=$(this).attr("id").substring(4);
        var count=0;
        $(this).find('input').each(function(){
        if($(this).val() == ''){
            $(this).addClass('empty');
            count++;
            empty++;
           }else{
            $(this).removeClass('empty');
           }
        });
        $(this).find('textarea').each(function(){
        if($(this).val() == ''){
            $(this).addClass('empty');
            count++;
            empty++;
           }else{
            $(this).removeClass('empty');
           }
        });
        if(count>0){
            $('a[href=#item'+ id +']').addClass('emptyList');
        }else{
            $('a[href=#item'+ id +']').removeClass('emptyList');
        }
    });
                         
}
function checkListEmpty(){
    
    $("#formPanel").find('[id*="item"]').each(function(){
        var id=$(this).attr("id").substring(4);
        var count=0;
        $(this).find('input').each(function(){
            if($(this).val() == ''){
                count++;
               }
        });
        $(this).find('textarea').each(function(){
            if($(this).val() == ''){
                count++;
               }
        });
        if(count>0){
            $('a[href=#item'+ id +']').addClass('emptyList');
        }else{
            $('a[href=#item'+ id +']').removeClass('emptyList');
        }
    });
                         
}
function checkEmpty(me,add=''){
    var me = me;
    if(me.value==''){
        me.className = "form-control "+ add +" empty";
    }else{
        me.className = "form-control "+ add;
    }
    empty++;
    checkListEmpty();
}

function checkEmptyPoints(me){
    var me = me;
    if(me.value==''){
        me.className = "quizPoints empty";
    }else{
        me.className = "quizPoints ";
    }
    empty++;
    checkListEmpty();
}
var saved=[];

function saveStorage(){
    if(localStorage.autosave){
       localStorage.removeItem('autosave');
       }
    localStorage.setItem('autosave', JSON.stringify(items));
    saved = JSON.parse(localStorage.getItem('autosave'));
}


setInterval(function() {
        arraySave();
}, 30 * 1000); // 60 * 1000 milsec