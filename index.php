<?php
session_start();
include('headerLink.php');
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="icon" type="image/png" href="img/favicon1.png" >
    <meta name="description" content="CSJPIIAS E-LEARNING" />
    <meta name="author" content="KENNETH CAMBAYA" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>CSJPIIAS E-LEARNING</title>
    <!-- BOOTSTRAP STYLE SHEET -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS STYLE SHEET -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES -->
    <link href="assets/css/style.css" rel="stylesheet" />
	<!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png" alt="" class="img-responsive" height="70px" width="70px" />
                </a>
            </div>
            <div class="navbar-collapse collapse navbar-right scroll-me">
                <ul class="nav navbar-nav ">
                    <li><a href="#home">HOME</a></li>
                    <li><a href="#download">DOWNLOAD</a></li>
                    <li><a href="#developers">TEAM</a></li>
                    <li><a href="#contact">CONTACT</a></li>
                    <li><a href="signin/" class="signin">SIGN IN</a></li>
                </ul>
            </div>

        </div>
    </div>
    <!-- NAVBAR CODE END -->
    <div id="home">
        <div class="overlay">
            <!-- overylay class usage -->
            <div class="container">
                <div class="col-md-8 col-md-offset-2 text-center scroll-me">

                    <h1>CSJPII</h1>
                    <h4>E-Learning</h4>
                    <p class="p-cls">
                        Download our Mobile App now. Cause we made it easier for you!
                    </p>
                    <a href="#download" class="btn btn-default btn-download">Download Now</a>
                </div>

            </div>
        </div>

    </div>
    <!--HOME SECTION END  -->
    <section id="download">
        <div class="container">
            <div class="row text-center pad-bottom">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                    <h2 class="head-set">DOWNLOAD</h2>
                    <p>
                        Our Mobile App is only available for ANDROID and WINDOWS platform mobile devices.
                    </p>
                </div>
            </div>
            <div class="row text-center">

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <i class="fa fa-android  fa-5x"></i>
                    <h4 class="head-set">ANDROID</h4>
                    <p>
                        You can download our Android version <a href="https://build.phonegap.com/apps/2551232/download/android/?qr_key=WAw5vrFeAuxsy5qdt_ey">here</a>.
                    </p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <i class="fa fa-windows  fa-5x"></i>
                    <h4 class="head-set">WINDOWS</h4>
                    <p>
                        You may also download our Windows version <a href="">here</a>.
                    </p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <img src="assets/img/qrcode.JPG" width="70px" height="70px"alt="QR Code">
                    <h4 class="head-set">QR CODE</h4>
                    <p>
                        Or you can use your QR Code Scanner here.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--SERVICES SECTION END  -->
    <div class="parallax-like">
        <div class="overlay">


            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div>
                            <strong>1000+</strong>
                            <p>
                                Students
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div>
                            <strong>10+</strong>
                            <p>
                                Programs
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div>
                            <strong>20+</strong>
                            <p>
                                Employees
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--PARALLAX LIKE / STATS SECTION END  -->
    <section id="developers">
        <div class="container">
            <div class="row text-center pad-bottom">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                    <h2 class="head-set">Project Team</h2>
                    <p>
                        <!--Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                         Curabitur nec nisl odio. Mauris vehicula at nunc id posuere.-->
                    </p>
                </div>
            </div>
            <div class="row text-center portfolio-item">

                <div class="col-md-4 col-sm-4 ">
                    <div class="alert alert-info">
                        <img src="assets/img/1.jpg" class="img-circle img-dev" alt="Kenneth Cambaya" />

                        <h3>Kenneth A. Cambaya</h3>
                        <h5>Senior Developer/Team Leader</h5>
                        <p><a href="https://www.facebook.com/youpuppey"><i class="fa fa-facebook-square fa-2x"></i></a></p>
                    </div>

                </div>
                <div class="col-md-4 col-sm-4 ">
                    <div class="alert alert-danger">
                        <img src="assets/img/2.jpg" class="img-circle" width="200px" height="200px" alt="Joseph Domanico" />

                        <h3>Mark Joseph G. Domanico</h3>
                        <h5>Software Engineer/Co-Leader</h5>
                        <p><a href="https://www.facebook.com/Mark2422"><i class="fa fa-facebook-square fa-2x"></i></a></p>
                    </div>

                </div>
                <div class="col-md-4 col-sm-4 ">
                    <div class="alert alert-success">
                        <img src="assets/img/3.jpg" class="img-circle" width="200px" height="200px" alt="Renz Lorenzo" />

                        <h3>John Renz C. Lorenzo</h3>
                        <h5>Software Engineer/ Member</h5>
                        <p><a href="https://www.facebook.com/oznerolzner"><i class="fa fa-facebook-square fa-2x"></i></a></p>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!--WORKS / PORTFOLIO SECTION END  -->
    <div id="video-sec" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=Ycv5fNd4AeM',containment:'self',autoPlay:true, mute:true, startAt:0, opacity:1,mute: true,showControls:false}" alt="Map Location">
        <!-- change https://www.youtube.com/watch?v=Ycv5fNd4AeM to your youtube url-->
        <div class="overlay">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h1>HERE IS BACKGROUND VEDIO</h1>
                        <h4>This scripts uses plugin <strong>jquery.mb.YTPlayer</strong>

                        </h4>
                        <h4>to play video in background of divs

                        </h4>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- VIDEO SECTION END-->
    <section id="contact">
        <div class="container">
            <div class="row text-center ">
               <div class="col-md-6 col-sm-6">
                   <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3861.2336805501977!2d121.1141637!3d14.5857561!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c78676c9563b%3A0xe460950dd9c4987d!2sCollege+of+Saint+John+Paul+II+Arts+and+Sciences!5e0!3m2!1sen!2sph!4v1491187826221" width="90%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h2 class="head-set">OUR LOCATION</h2>
                    <br />
                    <h3>Mercedez Bldg, Ortigas Ave Ext,</h3>
                    <h3>Cainta, 1900 Rizal, Philippines</h3>
                    <h3><strong>Email :</strong> kencambaya@gmail.com</h3>
                </div>
            </div>
        </div>
    </section>
    <!-- CONTACT SECTION END-->
    <section id="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/logo.png" alt="" class="img-responsive" />
            </div>
            <div class="col-md-6">
                <i class="fa fa-facebook-square fa-5x fa-spin"></i>
            </div>
        </div>

    </div>
        </section>
    <!-- CLIENTS SECTION END-->
    <footer>
        &copy; 2017 CSJPII.ESY.ES  | <a href="http://www.facebook.com/youpuppey" target="_blank">by Kenneth A. Cambaya</a>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- REQUIRED SCRIPTS FILES -->
    <!-- CORE JQUERY FILE -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- REQUIRED BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- BACKGROUND VIDEO PLUGIN  -->
    <script src="assets/js/jquery.mb.YTPlayer.js"></script>
    <!-- SCROLLING SCRIPTS PLUGIN  -->
    <script src="assets/js/jquery.easing.min.js"></script>
    <!-- CUSTOM SCRIPTS   -->
    <script src="assets/js/custom.js"></script>

</body>

</html>
