<!DOCTYPE HTML>
<html>
<head>
<style>
p {
  text-align: center;
  font-size: 60px;
}
</style>
</head>
<body>
<div id="startValuesAndTargetExample">
    <div class="values"></div>
    <div class="progress_bar">.</div>
</div>
<script src="js/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/easytimer.js"></script>
<script>
var timer = new Timer();
timer.start({precision: 'seconds', startValues: {seconds: 90}, target: {seconds: 120}});
$('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString());
timer.addEventListener('secondsUpdated', function (e) {
    $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString());
    
});
timer.addEventListener('targetAchieved', function (e) {
    $('#startValuesAndTargetExample .progress_bar').html('COMPLETE!!');
});

</script>

</body>
</html>
